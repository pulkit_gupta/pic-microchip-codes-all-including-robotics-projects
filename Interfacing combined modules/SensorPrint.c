/////////////////////////////////
// Program: Sensor to LCD      //
// Author: Pulkit Gupta        //
// Email: pulkit.itp@gmail.com //
/////////////////////////////////




#include <p18cxxx.h>
#include<adc.h>
#include<LCD.h>

#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}

#pragma code


// Initializing
#define ms()							TRISAbits.TRISA1=1;TRISAbits.TRISA2=1;
#define ml()							TRISCbits.TRISC0=0;TRISCbits.TRISC1=0;TRISCbits.TRISC2=0;TRISDbits.TRISD1=0;
#define mm()							TRISBbits.TRISB0=0;TRISBbits.TRISB1=0;TRISBbits.TRISB2=0;TRISBbits.TRISB3=0;

#define SensorLeft						PORTAbits.RA0
#define SensorRight						PORTAbits.RA2


#define l1							PORTDbits.RD1
#define l2							PORTCbits.RC0

#define l3						PORTCbits.RC1
#define l4						PORTCbits.RC2

#define m1								PORTBbits.RB3
#define m2								PORTBbits.RB2
#define m3								PORTBbits.RB1
#define m4								PORTBbits.RB0


void init()
{
	ADCON1 = 0x0F;                // Default all pins to digital
	/*
		TRISBbits.TRISB5=0;
		TRISBbits.TRISB7=0;
		TRISDbits.TRISD4=0;
		TRISDbits.TRISD5=0;
		TRISDbits.TRISD6=0;
		TRISDbits.TRISD7=0;
	*/	
	
	pinset();   //ALL CONNECTIONS WITH PIC
	init_LCD();  //initializing lcd
	Delay_s(100);
	cls_LCD();
	
	ms();
	ml();
	mm();
   //ADC begins
	OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH0 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH1 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH2 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	Delay_s( 50 ); // Delay for 50
	//ADC ends
}


void int2string(int num, char * str) 
{
	int i ;
	for ( i = 2 ; i >= 0 ; i-- ) 
	{
		str[i] = num % 10 + '0' ;
		num /= 10 ;
	}
	str[3] = '\0' ;
}

int srd =0;
int sld =0;
int spd =0;


int sr()
{
	SetChanADC( ADC_CH1 );
	ConvertADC(); // Start conversion
	while( BusyADC() ); // Wait for completion
 	srd = ReadADC(); // Read result
	return srd;
}

int sl()
{
	SetChanADC( ADC_CH2 );
	ConvertADC(); // Start conversion
	while( BusyADC() ); // Wait for completion
 	sld = ReadADC(); // Read result
	return sld;
}

int sp()
{
	SetChanADC( ADC_CH0 );
	ConvertADC(); // Start conversion
	while( BusyADC() ); // Wait for completion
 	spd = ReadADC(); // Read result
	return spd;
}

void code()
{

}



void sensevalue()
{
	char pulkit[]="swefkj!+-oemdi" ;  //initializing few characters
	
	int2string(sp(),  pulkit);
	WriteChar4('P');
	Delay_s(1);
	WriteString(pulkit , 3);
	Delay_s(1);
	WriteChar4(' ');
	Delay_s(1);

	
	int2string(sl(),  pulkit);
	WriteChar4('L');
	Delay_s(1);
	WriteString(pulkit , 3);
	Delay_s(1);
	WriteChar4(' ');
	Delay_s(1);
	
	
	int2string(sr(),  pulkit);
	WriteChar4('R');
	Delay_s(1);
	WriteString(pulkit , 3);
	Delay_s(1);
	WriteChar4(' ');
	Delay_s(1);
	
	Delay_s(200);  //refresh time for LCD
	cls_LCD();
}



void main()
{
	init();
	while(1)
	{
		sensevalue();
		code();
	}

}



//end main

/** EOF Demo02.c *************************************************************/
