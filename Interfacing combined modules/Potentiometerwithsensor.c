
/*
Program: Sensor working with Potentiometer
Author: Pulkit Gupta
Email: pulkit.itp@gmail.com
*/





#include <p18cxxx.h>
#include<adc.h>

#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}
#pragma code
#pragma code




// Sensor Inputs

#define mInitAllSensors()				TRISAbits.TRISA1=1;TRISAbits.TRISA2=1;
#define mInitAllLEDs()					TRISCbits.TRISC0=0;TRISCbits.TRISC1=0;TRISCbits.TRISC2=0;TRISDbits.TRISD1=0;
#define mInitAllMotors()				TRISBbits.TRISB0=0;TRISBbits.TRISB1=0;TRISBbits.TRISB2=0;TRISBbits.TRISB3=0;

#define SensorLeft						PORTAbits.RA0
#define SensorRight						PORTAbits.RA2


#define l1								PORTDbits.RD1
#define l2								PORTCbits.RC0

#define l3								PORTCbits.RC1
#define l4								PORTCbits.RC2

#define M1								PORTBbits.RB3
#define M2								PORTBbits.RB2
#define M3								PORTBbits.RB1
#define M4								PORTBbits.RB0


void Delay( long int x)
{
	unsigned int i,j;
	for(i=0;i<x;i++)
	{
		for(j=0;j<1024;j++)
		{}
	}//external for
}



void init()
{
ADCON1 = 0x0F;                // Default all pins to digital
mInitAllSensors();
mInitAllLEDs();
mInitAllMotors();


	//ADC begins

	OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH0 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH1 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH2 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	Delay( 50 ); // Delay for 50
	//ADC ends
}//init finish




int sensorR =0;
int sensorL =0;
int sensorP =0;


int SensorR()
{
	SetChanADC( ADC_CH1 );
	ConvertADC(); // Start conversion
	while( BusyADC() ); // Wait for completion
	 sensorR = ReadADC(); // Read result
}

int SensorL()
{
	SetChanADC( ADC_CH2 );
	ConvertADC(); // Start conversion
	while( BusyADC() ); // Wait for completion
	 sensorL = ReadADC(); // Read result
}

int SensorP()
{
	SetChanADC( ADC_CH0 );
	ConvertADC(); // Start conversion
	while( BusyADC() ); // Wait for completion
	 sensorP = ReadADC(); // Read result
}


void forward ()
{
	M1=1;
	M2=0;
	M3=1;
	M4=0;
}

void backward ()
{
	M1=0;
	M2=1;
	M3=0;
	M4=1;
}

void left ()
{
	M1=0;
	M2=1;
	M3=1;
	M4=0;
}

void right ()
{
	M1=1;
	M2=0;
	M3=0;
	M4=1;
}


void stop ()
{
	M1=0;
	M2=0;
	M3=0;
	M4=0;
}



void code()
{

	if(SensorR()>SensorP())
	{l1=1;l2=1;right();}

	else
	{l1=0;l2=0;}


	if(SensorL()>SensorP())
	{l3=1;l4=1;left();}

	else
	{l3=0;l4=0; stop();}

}



void main()
{
	init();
	Delay(100);
	code();
}



//end main

/** EOF Demo02.c *************************************************************/
