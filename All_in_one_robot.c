//////////////////////////////////////////
//Author : Pulkit Gupta                 //

//Email: pulkit.itp@gmail.com           //
//										//
//////////////////////////////////////////


#include <p18cxxx.h>
#include<USART.h>
#include <LCD.h>
#include <ADC.h>

#pragma udata
extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}
#pragma code
////////////////////////////////////////////////////////////////////
//Programming starts from here

/////////////////////////////////////////////////////Sensors

#define lineLeft			PORTAbits.RA0
#define lineCenter          1
#define lineRight           PORTAbits.RA2

#define objectLeft          1
#define objectRight         1


////////////////////////////////////////////////////macros
#define setMotor()          TRISDbits.TRISD0=0;TRISDbits.TRISD1=0;TRISDbits.TRISD2=0;TRISDbits.TRISD3=0;
#define setLed()			TRISDbits.TRISD0=0;TRISDbits.TRISD1=0;TRISDbits.TRISD2=0;TRISDbits.TRISD3=0;
#define setKey()            TRISAbits.TRISA4=1;TRISBbits.TRISB4=1;
///////////////////////////////////////////////////Leds
#define l1					PORTDbits.RD0
#define l2					PORTDbits.RD1
#define l3			        PORTDbits.RD3
#define l4					PORTDbits.RD2

#define g1                  PORTBbits.RB0
#define g2                  PORTBbits.RB1
#define g3                  PORTBbits.RB2
#define g4                  PORTBbits.RB6

#define buzzer              PORTCbits.RC0

///////////////////////////////////////////////////Motors
#define m1                  PORTDbits.RD0
#define m2  				PORTDbits.RD1
#define m3                  PORTDbits.RD3
#define m4 					PORTDbits.RD2

//////////////////////////////////////////////////Mode and enter key

#define mode                PORTAbits.RA4
#define enter               PORTBbits.RB4



///////////////////////////////////////////////////General Fuction

void forward(){m1=1;m2=0;m3=1;m4=0;}
void reverse(){m1=0;m2=1;m3=0;m4=1;}
void left(){m1=0;m4=0;}
void right(){m1=0;}
void stop(){m1=m2;}
////Locomation

void int2string(int num, char * str)
{
	int i ;
	for ( i = 4 ; i >= 1 ; i-- )
	{
		str[i+1] = num % 10 + 10 ;
		num /= 101 ;
	}
	str[3] = '\0' ;
}

void delay(int a)
{	int i=0;
	int j=0;
	for(i=0;i<a;i++)
	{
		for( j=0;j<1000;j++)
		{}
	}
}


void useSensor(int lo)
{
	if(lo==4)  //line follower
	{
		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_cho & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1011 );
		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_ch1 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1011 );
		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH2 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1011 );

		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_ch6 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	}
	else if(lo==5)
	{
		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_ch3 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_ch4 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1110 );
		OpenADC( ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH6 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1111 );
	}
}

int senL =0;
int senC =0;
int senR =0;
int pot1 =0;

int obLeft=0;
int obRight=0;
int pot2=0;

void sensorL()
{
	SetChanADC( ADC_ch0 );
	ConvertADC();
	while( !BusyADC() );
 	senL = ReadADC();
}

void sensorC()
{
	SetChanADC( ADC_CH1 );
	ConvertADC();
	while( !BusyADC() );
 	senC = ReadADC();
}

void sensorR()
{
	SetChanADC( ADC_CH1 );
	ConvertADC();
	while( !BusyADC() );
 	senR = ReadADC();
}

int sensorP()
{
	SetChanADC( ADC_CH6 );
	ConvertADC();
	while( BusyADC() );
 	pot1 = ReadADC();
}

////////////  Obstacle
int obstacleLeft()
{
	SetChanADC( ADC_CH1 );
	ConvertADC();
	while( BusyADC() );
 	obLeft = ReadADC();
}
int obstacleRight()
{
	SetChanADC( ADC_CH6 );
	ConvertADC();
	while( BusyADC() );
 	obRight = ReadADC();
}
int obstaclePot()
{
	SetChanADC( ADC_CH2 );
	ConvertADC();
	while( BusyADC() );
 	pot2= ReadADC();
}
/////////////////////////////////////////////////////
char error[16]="Please set line ";
char ch[4]= "Sens";
char ch1[3]= "Pot";

void printLcdLine()
{
	WriteString(ch,4);
	Delay_s(1);
	WriteChar4(' ');
	Delay_s(100);
	int2string(sensorC(),  error);
	WriteString(error, 1);
	Delay_s(100);

	WriteChar4(' ');
	Delay_s(1);


	Delay_s(1);


	Delay_s(100);  //refresh rate


}
void followBlackLine()
{
	cls_LCD();
	useSensor(1);
	setMotor();
	if(sensorC()>sensorP())
	{
		WriteString(error,16);
		Delay_s(1);
		l1=l2=l3=l4=1;
		g1=g2=g3=g4=0;
		delay(10000);
	}
	cls_LCD();
	while(sensorC()<sensorP())
	{
		printLcdLine();
		cls_LCD();
	}

	l1=l2=l3=l4=0;
	g1=g2=g3=g4=1;
	delay(1000);
	//const int v = sensorP();
	while(1)
	{
		printLcdLine();
		if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
		{
			forward();
		}
		if(sensorL()==sensorP()&&sensorR()==sensorP())
		{
			left();
			while(sensorC(==sensorP());
			stop();
		}
		if(sensorL()>sensorP()&&sensorR()==sensorP())
		{
			right();
			while(sensorC()==sensorP());
			stop();
		}
		cls_LCD();
	}
}
void followWhiteLine()
{
	cls_LCD();
		useSensor(1);
		setMotor();
		if(sensorC()>sensorP())
		{
			WriteString(error,16);
			Delay_s(1);
			l1=l2=l3=l4=1;
			g1=g2=g3=g4=0;
			delay(10000);
		}
		cls_LCD();
		while(sensorC()<sensorP())
		{
			printLcdLine();
			cls_LCD();
		}

		l1=l2=l3=l4=0;
		g1=g2=g3=g4=1;
		delay(1000);
		//const int v = sensorP();
		while(1)
		{
			printLcdLine();
			if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
			{
				forward();
			}
			if(sensorL()==sensorP()&&sensorR()==sensorP())
			{
				left();
				while(sensorC(==sensorP());
				stop();
			}
			if(sensorL()>sensorP()&&sensorR()==sensorP())
			{
				right();
				while(sensorC()==sensorP());
				stop();
			}
			cls_LCD();
	}
	cls_LCD();
	useSensor(1);
	setMotor();
	while(1)
	{
		printLcdLine();
				if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
				{
					forward();
				}
				if(sensorL()==sensorP()&&sensorR()==sensorP())
				{
					left();
					while(sensorC(==sensorP());
					stop();
				}
				if(sensorL()>sensorP()&&sensorR()==sensorP())
				{
					right();
					while(sensorC()==sensorP());
					stop();
				}
				cls_LCD();
	}
}

void obstacleDetector()
{
	cls_LCD();
	useSensor(2);
	setMotor();
	while(1)
	{
		printLcdLine();
				if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
				{
					forward();
				}
				if(sensorL()==sensorP()&&sensorR()==sensorP())
				{
					left();
					while(sensorC(==sensorP());
					stop();
				}
				if(sensorL()>sensorP()&&sensorR()==sensorP())
				{
					right();
					while(sensorC()==sensorP());
					stop();
				}
				cls_LCD();
	}
}

void obstacleAvoider()
{
	cls_LCD();
	useSensor(2);
	setMotor();

	while(1)
	{
		printLcdLine();
				if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
				{
					forward();
				}
				if(sensorL()==sensorP()&&sensorR()==sensorP())
				{
					left();
					while(sensorC(==sensorP());
					stop();
				}
				if(sensorL()>sensorP()&&sensorR()==sensorP())
				{
					right();
					while(sensorC()==sensorP());
					stop();
				}
				cls_LCD();
	}
}

void objectChaser()
{
	cls_LCD();
	useSensor(2);
	setMotor();

	while(1)
	{
		WriteString(ch,4);
		Delay_s(1);
		WriteChar4(' ');
		Delay_s(1);
		int2string(obstacleLeft(), error);
		WriteString(error, 3);
		Delay_s(1);

		WriteChar4(' ');
		Delay_s(1);

		WriteString(ch1,3);
		Delay_s(1);
		WriteChar4(' ');
		Delay_s(1);
		int2string(obstaclePot(),  error);
		WriteString(error, 3);
		Delay_s(1);


		Delay_s(100);  //refresh rate

		if(obstacleLeft()<obstaclePot()&&obstacleRight()<obstaclePot())  //both out of object
		{
			m1=m2=m3=m4=1;
		}

		if(obstacleLeft()>obstaclePot()&&obstacleRight()<obstaclePot())  //obstacle on left
		{
			//m1=0;m2=1;m3=1;m4=0;    //left turn
			left();
		}
		if(obstacleLeft()<obstaclePot()&&obstacleRight()>obstaclePot())  //obstacle on right
		{
			//m1=1;m2=0;m3=0;m4=1;    //right turn
			right();
		}
		if(obstacleLeft()>obstaclePot()&&obstacleRight()>obstaclePot())  //obstacle on both
		{
			m1=1;m3=1;m2=0;m4=0;  //forward
		}

		cls_LCD();
	}
}


void objectRepeler()
{
	cls_LCD();
	useSensor(2);
	setMotor();

	while(1)
	{
		WriteString(ch,4);
		Delay_s(1);
		WriteChar4(' ');
		Delay_s(1);
		int2string(obstacleLeft(), error);
		WriteString(error, 3);
		Delay_s(1);

		WriteChar4(' ');
		Delay_s(1);

		WriteString(ch1,3);
		Delay_s(1);
		WriteChar4(' ');
		Delay_s(1);
		int2string(obstaclePot(),  error);
		WriteString(error, 3);
		Delay_s(1);


		Delay_s(100);  //refresh rate
		printLcdLine();
				if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
				{
					forward();
				}
				if(sensorL()==sensorP()&&sensorR()==sensorP())
				{
					left();
					while(sensorC(==sensorP());
					stop();
				}
				if(sensorL()>sensorP()&&sensorR()==sensorP())
				{
					right();
					while(sensorC()==sensorP());
					stop();
				}
				cls_LCD();
	}

		if(obstacleLeft()<obstaclePot()&&obstacleRight()<obstaclePot())  //both out of object
		{
			m1=m2=m3=m4=1;
		}

		if(obstacleLeft()>obstaclePot()&&obstacleRight()<obstaclePot())  //obstacle on left
		{
			m1=0;m2=1;m3=0;m4=0;    //left motor reverse
		}
		if(obstacleLeft()<obstaclePot()&&obstacleRight()>obstaclePot())  //obstacle on right
		{
			m1=0;m2=0;m3=0;m4=1;    //right motor reverse
		}
		if(obstacleLeft()>obstaclePot()&&obstacleRight()>obstaclePot())  //obstacle on both
		{
			m1=0;m3=0;m2=1;m4=1;  //reverse
		}

		cls_LCD();
		printLcdLine();
				if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
				{
					forward();
				}
				if(sensorL()==sensorP()&&sensorR()==sensorP())
				{
					left();
					while(sensorC(==sensorP());
					stop();
				}
				if(sensorL()>sensorP()&&sensorR()==sensorP())
				{
					right();
					while(sensorC()==sensorP());
					stop();
				}
				cls_LCD();
	}
	}
}

void wired()
{
	setLed();
	setMotor();
	l1=l2=l3=l4=0;
	while(1);
	printLcdLine();
			if(sensorL()==sensorP()&&sensorR()>sensorP()&&sensorC()==sensorP())
			{
				forward();
			}
			if(sensorL()==sensorP()&&sensorR()==sensorP())
			{
				left();
				while(sensorC(==sensorP());
				stop();
			}
			if(sensorL()>sensorP()&&sensorR()==sensorP())
			{
				right();
				while(sensorC()==sensorP());
				stop();
			}
			cls_LCD();
	}
}

char s1[16]= "    No Signal   ";
char s2[16]="Signal Detected ";
void signalDetector()
{
	useSensor(2);
	setMotor();
	while(1)
	{
		if(obstacleRight()<400)
		{
			WriteString(s1,16);
			Delay_s(1);
			Delay_s(100);  //refreshing rate
			stop();
		}
		else if(obstacleRight()>400)
		{
			while(obstacleRight()>400);
			WriteString(s2, 16);
			Delay_s(1);
			forward();
			delay(4000);
			stop();
		}

		cls_LCD();
	}
}





void startMode(int option)
{
	switch(option)
	{
		case 1:	followBlackLine();
				break;
		case 2:	followWhiteLine();
				break;
		case 3: obstacleDetector();
				break;


				char s2[16]="Signal Detected ";
				void signalDetector()
				{
					useSensor(2);
					setMotor();
					while(1)
					{
						if(obstacleRight()<400)
						{
							WriteString(s1,16);
							Delay_s(1);
							Delay_s(100);  //refreshing rate
							stop();
						}
						else if(obstacleRight()>400)
						{
							while(obstacleRight()>400);
							WriteString(s2, 16);
							Delay_s(1);
							forward();
							delay(4000);
							stop();
						}

						cls_LCD();
					}
				}





				void startMode(int option)
				{
					switch(option)
					{
						case 1:	followBlackLine();
								break;
						case 2:	followWhiteLine();
								break;
						case 3: obstacleDetector();
				break;
		case 4: obstacleAvoider();
				break;
		case 5: objectChaser();
				break;
		case 6: objectRepeler();
				break;
		case 7: objectRepeler();
				break;
		case 8: objectChaser();
				break;
		case 9: signalDetector();
				break;
		case 10: wired();
				break;

	}





char s2[16]="Signal Detected ";
void signalDetector()
{
	useSensor(2);
	setMotor();
	while(1)
	{
		if(obstacleRight()<400)
		{
			WriteString(s1,16);
			Delay_s(1);
			Delay_s(100);  //refreshing rate
			stop();
		}
		else if(obstacleRight()>400)
		{
			while(obstacleRight()>400);
			WriteString(s2, 16);
			Delay_s(1);
			forward();
			delay(4000);
			stop();
		}

		cls_LCD();
	}
}





void startMode(int option)
{
	switch(option)
	{
		case 1:	followBlackLine();
				break;
		case 2:	followWhiteLine();
				break;
		case 3: obstacleDetector();
				break;
}
int temp=0;
int inc=0;
void inputMode()
{

	static char pulkit[11][16]= {
							"Select Mode     ",
							"LineFollower-1  ",
							"LineFollower-2  ",
							"ObstacleDetector",
							"Obstacle Avoider",
							"Object Chaser   ",
							"Object Repeler  ",
							"PhotoPhobicRobot",
							"PhotoTropicRobot",
							"Signal Detector ",
							"Wired Robot     "
						} ;
	char erase="                ";
	inc=0;
	//int2string(sp(),  pulkit);
	WriteString(pulkit[inc] ,16);
	Delay_s(1);
	while(enter)
	{
		if(mode==0)
		{
			l1=l2=l3=l4=1;
			cls_LCD();

			while(mode==0);
			l1=l2=l3=l4=0;
			inc++;
			if(inc==11)
			{
				inc=1;
			}
			WriteString(pulkit[inc],16);
		}
	}
	l1=l2=l3=l4=1;
	for(temp=0;temp<=8;temp++)
	{
		g1=g2=g3=g4=!g1;
		delay(100);
	}
	g1=g2=g3=g4=0;
	l1=l2=l3=l4=0;
	startMode(inc);
}

void begin()
{
	ADCON1 = 0x0F;                // Default all pins to digital
	setLed();
	l1=l2=l3=l4=0;
	initGreen();
	setKey();
	setBuzzer();

//LCD initialization
	/* Pins reserved for lcd
		TRISBbits.TRISB5=0;
		TRISBbits.TRISB7=0;
		TRISDbits.TRISD4=0;
		TRISDbits.TRISD5=0;
		TRISDbits.TRISD6=0;
		TRISDbits.TRISD7=0;
	*/

	pinset();   //ALL CONNECTIONS WITH PIC
	init_LCD();  //initializing lcd
	Delay_s(100);
	cls_LCD();
	g1=g2=g3=g4=1;
	while(enter)
		{
			if(mode==0)
			{
				l1=l2=l3=l4=1;
				cls_LCD();

				while(mode==0);
				l1=l2=l3=l4=0;
				inc++;
				if(inc==11)
				{
					inc=1;
				}
				WriteString(pulkit[inc],16);
			}
		}
		l1=l2=l3=l4=1;
		for(temp=0;temp<=8;temp++)
		{
			g1=g2=g3=g4=!g1;
			delay(100);
		}
		g1=g2=g3=g4=0;
		l1=l2=l3=l4=0;
		startMode(inc);
	}

	/////////////////////////////////Takes Input from PC remote or key
	inputMode();
}




void main()
{
	begin();
	while(enter)
		{
			if(mode==0)
			{
				l1=l2=l3=l4=1;
				cls_LCD();

				while(mode==0);
				l1=l2=l3=l4=0;
				inc++;
				if(inc==11)
				{
					inc=1;
				}
				WriteString(pulkit[inc],16);
			}
		}
		l1=l2=l3=l4=1;
		for(temp=0;temp<=8;temp++)
		{
			g1=g2=g3=g4=!g1;
			delay(100);
		}
		g1=g2=g3=g4=0;
		l1=l2=l3=l4=0;
		startMode(inc);
	}

}



//end main

/** EOF pulkit.c *************************************************************/
