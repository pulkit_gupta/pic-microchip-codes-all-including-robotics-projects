/*
 Task: Blink led connected on portb pin 6
 Programmer: Pulkit Gupta
 Queries: pulkit.itp@gmail.com 
*/

#include"p18f4550.h"
#include"delays.h"
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800

void _reset (void)
{    _asm goto _startup _endasm
}
#pragma code

void delay();
void main (void)
{  /* Make all bits on the Port B (LEDs) output bits.
   * If bit is cleared, then the bit is an output bit.*/   
	ADCON1 |= 0x0F;
	TRISB=0;
	PORTB=0;
	while(1)
	{
		PORTBbits.RB6=1;
		delay();
		PORTBbits.RB6=0;
		delay();
	}
}
void delay()
{
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
}  // Its is prefferable to use delay function in discrete form
