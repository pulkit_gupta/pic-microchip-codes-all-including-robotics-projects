/*
Task: Print HELLO on LCD
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/
#include"p18f4550.h"
#include"delays.h"
#include"lcd.h"
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}
#pragma code

/* CTRL_PORT defines the port where the control lines are connected.
 * These are just samples, change to match your application.
 */
//#define UPPER

void main( void )
{
	char data[]="Hello";
	ADCON1 = 0x0F;
	pinset();
	init_LCD();
	Delay_s(100);
	cls_LCD();
	while(1)
	{
		WriteString(data , 5);
		Delay_s(200);
		cls_LCD();
	}
}
