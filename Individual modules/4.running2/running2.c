/*
Task: running led connected on portb from B7 to B0
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/
#include"p18f4550.h"
#include"delays.h"
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{    _asm goto _startup _endasm
}
#pragma code

void delay();
void main (void)
{
	/* Make all bits on the Port B (LEDs) output bits.
   * If bit is cleared, then the bit is an output bit.*/
	int temp=8;
	ADCON1 |= 0x0F;
	TRISB=0;
	PORTB=1;
	while(1)
	{
		PORTB=temp;
		delay();
		delay();
		temp>>=1;
		if(temp==0)
		temp=8;
	}
}

void delay()
{
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
}
