/*
Task: Print dynamic data on LCD
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/

#include"p18f4550.h"
#include"delays.h"
#include"lcd.h"
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{    _asm goto _startup _endasm
}
#pragma code
void int2string(int num, char * str)
{	int i ;
	for ( i = 2 ; i >= 0 ; i-- )
	{	str[i] = num % 10 + '0' ;
		num /= 10 ;
	}str[3] = '\0' ;
}

void main( void )
{
	char data[]="MITBOTS";
	int count=0;
	ADCON1 = 0x0F;
	pinset();
	init_LCD();
	Delay_s(10);
	cls_LCD();
	while(1)
	{
		int2string(count, data);
		WriteChar4('C');Delay_s(1);		WriteChar4('O');Delay_s(1);		WriteChar4('U');Delay_s(1);
		WriteChar4('N');Delay_s(1);		WriteChar4('T');Delay_s(1);		WriteChar4('E');Delay_s(1);
		WriteChar4('R');Delay_s(1);		WriteChar4('   ');Delay_s(1);	WriteString(data , 3);
		Delay_s(200);
		cls_LCD();
		if(!PORTAbits.RA4)
		count++;
		if((!PORTBbits.RB4)&& count>0)
		count--;
	}
}
