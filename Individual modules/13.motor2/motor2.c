/*
Task: Run a dc motor and its direction changes when a button is pressed
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/

#include"p18f4550.h"
#include"delays.h"
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{    _asm goto _startup _endasm
}
#pragma code

void delay();
void main (void)
{
	/* Make all bits on the Port B (LEDs) output bits.If bit is cleared, then the bit is an output bit.*/
	int temp=255;
	ADCON1 |= 0x0F;
	TRISAbits.TRISA4 = 1;
	TRISC=0; TRISD=0;
	PORTC=0;
	PORTD=2;// INP1=0 and INP2=1
	while(1)
	{
		PORTCbits.RC1=1;		// ENABLE
		if(!PORTAbits.RA4)
		PORTD=PORTD^3;
		delay();delay();
	}
}

void delay()
{
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
	Delay1KTCYx(255);
}
