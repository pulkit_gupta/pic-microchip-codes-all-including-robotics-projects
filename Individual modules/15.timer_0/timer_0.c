/*
Task: Toggle the leds using timer 0 interrupt
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/

#include"p18f4550.h"
#include"delays.h"
#pragma udata
int count=0;
extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{  _asm goto _startup _endasm
}

#pragma code _INTERRUPT_VECTOR = 0x000808
void _interrupt_timer0()
{
	if (INTCONbits.TMR0IF)
	 {
	//	PORTB^=255;
		INTCONbits.TMR0IF = 0;
		count++;
		TMR0H = 0x0b;
 		TMR0L = 0xd1;
 	   	INTCONbits.TMR0IE=1;
		INTCONbits.PEIE=1;
		INTCONbits.GIEH = 1;
  	}
}
#pragma code

void main (void)
{
	int temp=0;
	ADCON1 |= 0x0F;
	T0CON = 0x82;
	INTCON = 0xE0;
	INTCON2 = 0x00;
	TMR0H = 0x0b;
	TMR0L = 0xd1;
	TRISB = 0;
	PORTB=0;
	while (1)
    {
		if(count==20)
		{
			count=0;
			PORTB^=255;
		}
	}
}
