/*
Task: Print string on LCD
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/

#include"p18f4550.h"
#include"delays.h"
#include"lcd.h"
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}
#pragma code

/* CTRL_PORT defines the port where the control lines are connected.
 * These are just samples, change to match your application.
 */


void main( void )
{
	char data[]="MITBOTS";
	ADCON1 = 0x0F;
	pinset();
	init_LCD();
	Delay_s(10);
	cls_LCD();
	while(1)
	{
			WriteString(data , 7);
		Delay_s(50);
		cls_LCD();
	}
}
