/*
Task: Print ADC channel 6 data on LCD
Programmer: Pulkit Gupta
Queries: pulkit.itp@gmail.com
*/

#include"p18f4550.h"
#include"delays.h"
#include"lcd.h"
#include<adc.h>
#pragma udata

extern void _startup (void);        // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{    _asm goto _startup _endasm
}
#pragma code

void int2string(int num, char * str)
{	int i ;
	for ( i = 3 ; i >= 0 ; i-- )
	{	str[i] = num % 10 + '0' ;
		num /= 10 ;
	}str[4] = '\0' ;
}

void main( void )
{
	char data[]="MITBOTS";
	int temp=0;
	ADCON1 = 0x0F;
	pinset();
	init_LCD();		Delay_s(10);		cls_LCD();
	OpenADC( ADC_FOSC_32 & ADC_RIGHT_JUST & ADC_1ANA , ADC_CH6 & ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS , 0b1010 );
	while(1)
	{
		SetChanADC( ADC_CH6 );
		ConvertADC(); // Start conversion
		while( BusyADC() ); // Wait for completion
		temp = ReadADC(); // Read result
		int2string(temp, data);
		WriteChar4('A');Delay_s(1);		WriteChar4('D');Delay_s(1);		WriteChar4('C');Delay_s(1);
		WriteChar4(' ');Delay_s(1);		WriteChar4('D');Delay_s(1);		WriteChar4('A');Delay_s(1);
		WriteChar4('T');Delay_s(1);		WriteChar4('A');Delay_s(1);		WriteChar4(' ');Delay_s(1);
		WriteString(data , 4);
		Delay_s(200);
		cls_LCD();
	}
}
