//This example shows that PWM function from PIC18F4550. There are 2 channels which can be used asPWM outputs.
//See from the schematic of PIC18F4550, these channels are noticed as CCP1 and CCP2. The PWM usestimer2 as time base.
//Before PWM works, timer2 must be set.
//The PWM period is calculated by:
//PWM Period = [period+1]*4*Tosc*(TMR2 Prescaler).
//PWM Dutycycle = (DCx<9:0>) x TOSC x (TMR2 prescaler).
//where DCx<9:0> is the 10-bit value specified in the call to this function.
//This example is using to PWM from both 2 channels to make fading LEDs.


#include <p18f4550.h>
#include <pwm.h> // include the PWM library
#include <delays.h> // include the delay library
#include <timers.h> // include the timer library


//Bootloader code, DO NOT DELETE!
extern void _startup (void);
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
_asm goto _startup _endasm
}
#pragma code

#pragma code _HIGH_INTERRUPT_VECTOR = 0x000808
void _high_ISR (void)
{
;
}
#pragma code
#pragma code _LOW_INTERRUPT_VECTOR = 0x000818
void _low_ISR (void)
{
;
}
#pragma code
int a; //the variable for the dutycycle.

//Delay functions
void Delay1mS(int x)
{
	//Pre: Delay library is included.
	//Post:Delay for x*1ms.
	int i;
	for (i=0; i<x; i++) Delay1KTCYx(12);
}


void Delay10mS(int x)
{
	//Pre: Delay library is included.
	//Post:Delay for x*10ms.
	int i;
	for (i=0; i<x; i++)
	Delay10KTCYx(12);
}


void Delay100mS(int x)
{
	//Pre: Delay library is included.
	//Post:Delay for x*100ms.
	int i;
	for (i=0; i<x; i++)
	Delay10KTCYx(120);
}
void Delay1S(int x)
{
	//Pre: Delay library is included.
	//Post:Delay for x*1s.
	int i;
	for (i=0; i<10*x; i++)
	Delay10KTCYx(120);
}

void main( void )
{
	//Pre: PWM and Delay library are included.
	//Post: The LED C1 and C2 change from dark to bright in 1.6second, and then fade to dark in1.6second.
	TRISC=0b11111001; // Set channel C1 and C2 as PWM output.
	LATC= 0x00; // Initialize the PORTC.as output

	OpenTimer2(T2_PS_1_16 & TIMER_INT_OFF); // Timer2 is used for the time base of the PWM. set timer2 before PWM works.

	// Set timer2 prescaler to 1:16, and set interrupts OFF

	OpenPWM1(159); // PWM period =[(period ) + 1] x 4 x TOSC x TMR2prescaler. The value of period is from 0x00 to 0xff.

	// Channel 1:PWM period = (159+1)*4*(1/48e6)*16 =0.21ms

	OpenPWM2(159); // Channel 2:PWM period = (159+1)*4*(1/48e6)*16 =0.21ms

	SetDCPWM1(640); //PWM x Duty cycle = (DCx<9:0>) x TOSC x TMR2 prescaler

	// Setting duty cycle for channel 1: DC = 640 *(1/48e6)*16 = 0.21 ms, duty cycle is 100% now

	SetDCPWM2(640); // Setting duty cycle for Channel 2: DC = 640 *(1/48e6)*16 = 0.21 ms, duty cycle is 100% now

	while(1) //Make fading LED
	{
		for(a=0;a<640; a=a+16) // Dutycycle increases in 1600 ms 0% --> 100%
		{
			SetDCPWM1(a); // give new value for the duty cycle.
			SetDCPWM2(a);
			Delay10mS(4);
		}
		for(a=0;a<640; a=a+16) // Dutycycle decreases in 1600 ms 100% --> 0%
		{
			SetDCPWM1(640-a);// give new value for the duty cycle.
			SetDCPWM2(640-a);
			Delay10mS(4);
		}
	}
	ClosePWM1(); // disable the PWM channel 1.
	ClosePWM2(); // disable the PWM channel 2.
}