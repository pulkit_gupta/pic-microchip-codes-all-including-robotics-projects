


/** I N C L U D E S **********************************************************/
#include <p18cxxx.h>
#include <delay.h>
#include<lcd.h>
/** V A R I A B L E S ********************************************************/
#pragma udata

/** P R I V A T E  P R O T O T Y P E S ***************************************/

/** V E C T O R  R E M A P P I N G *******************************************/


extern void _startup (void);        // See c018i.c in your C18 compiler dir
void ext0_isr (void);
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm;
}
#pragma code



#pragma code _HIGH_INTERRUPT_VECTOR = 0x000808
void _high_ISR (void)
{
    _asm GOTO ext0_isr _endasm;
}

#pragma code _LOW_INTERRUPT_VECTOR = 0x000818
void _low_ISR (void)
{
    ;
}
#pragma code



#pragma interrupt ext0_isr
long n=0;
void ext0_isr(void)
{
		n++;
		INTCONbits.INT0IF=0;
		WriteChar4(48+n);
		Delay_s(1);

}
/** D E C L A R A T I O N S **************************************************/
#pragma code
/******************************************************************************
 * Function:        void main(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Main program entry point.
 *
 * Note:            None
 *****************************************************************************/
/** L E D ***********************************************************/
void init()

{
	ADCON1 = 0x0F;                // Default all pins to digital
	pinset();   //initialize pins for lcd data transfer
	init_LCD();  //initializing lcd
	Delay_s(100);
	//cls_LCD();

}

void main()
{
	init();
	TRISC=0;
	PORTBbits.INT0=1;
	//INTCONbits.RBIE=1;
	INTCON2bits.INTEDG0=0;//falling edge
	INTCONbits.GIE = 1;//GLOBAL
	INTCONbits.INT0IE=1; //ENABLE
	INTCONbits.INT0IF=0;
	while(1)
	{

	}
}


