//*************************************************************************************************
//test code for int0, blink led on portD on receving interrupt

#include <p18f4450.h>
void main (void);
void InterruptHandlerHigh (void);


void main ()
{
	int i=0;
	INTCON2bits.INTEDG0=1;//m interrupt is rising edge
	INTCONbits.INT0IE=1;//m enable  interrupt
  	INTCONbits.GIEH = 1;          //enable global interrupts
  	TRISB = 1;			//set port b as input
	TRISD=0;			//set port d as output
	while(1);//move in loop, gets out on receving interrupt and then comes back.
}//end main




// High priority interrupt vector
#pragma code InterruptVectorHigh = 0x08
void InterruptVectorHigh (void)
{
  _asm
    goto InterruptHandlerHigh //jump to interrupt routine
  _endasm
}
//----------------------------------------------------------------------------
// High priority interrupt routine

#pragma code


#pragma interrupt InterruptHandlerHigh
void InterruptHandlerHigh ()
{
	int i=0;
	int j=0;
	if (INTCONbits.INT0F)
	{

	INTCONbits.INT0IE=0;//m disable the interrupt

		for(j=0;j<4;j++)//toggle the bit0 or portD on receiving interrupt
		{
			LATDbits.LATD0 = 1;
			for( i=0;i<40000;i++);
			LATDbits.LATD0 = 0;
			for( i=0;i<40000;i++);
		}

		INTCONbits.INT0F=0;	//m reset the flag
	}
INTCONbits.INT0IE=1;//m enable the interrupt
}

//----------------------------------------------------------------------------
//*************************************************************************************************